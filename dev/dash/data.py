from __future__ import division
from datasciutil.extraction import Extract
import datetime
import os
import pandas as pd

TOP100_customers = "'0adc5613-277f-4e3e-b0f4-6fefe122c5d3','103cedbc-262b-4c5e-94e0-f5d35c0a9c9d'," \
                   "'1672e2ed-5d4c-4b40-a29c-5ca85f44c53a','17cb7f39-3cfc-49ac-a2a4-6b91422ebe22'," \
                   "'1fa8c0d3-9909-4eda-b6d8-26e3de126629','24288b84-d9ca-499a-b1ca-5e65a4f42edf'," \
                   "'24eb6f9b-5f4f-4b57-be99-28c2403d1f86','255ec9ba-0fa0-4862-83fc-ac328f0e6260'," \
                   "'29326cd0-bf41-4c32-8893-d87bad71e065','33aeda48-f68c-4a04-8afd-d70185372a9e'," \
                   "'34fbdbed-f206-4191-b4c8-153cae3e73a5','365efd10-75ce-4a30-8d25-04f8c046813e'," \
                   "'3a4072bd-21d8-4924-9dcd-f0056ea6b2c3','433b77dd-6d45-452e-bd24-e29041bbf30c'," \
                   "'4c85b2e0-170d-4542-9622-164e13e2306f','4d5c9721-b34b-4574-beda-6a12cfe4cb4d'," \
                   "'528b967b-b40a-4766-a2ce-65afd24f3348','556184fe-0fb3-4681-bf76-7d3fa208d916'," \
                   "'56722794-d807-4498-a39f-d58ba0ab5b34','5753e3f6-09dc-43e9-b558-8a48cd5d3d86'," \
                   "'5a455302-e950-4895-a208-88f99252eb05','613d92ab-17cb-41e3-b32d-bdf2dbfe5ed5'," \
                   "'6749c577-269d-4c64-b6c5-bf599a0fcbcf','67f28690-1343-45e3-bd62-477eec30216b'," \
                   "'69441ef7-6e04-4511-9fe6-e41fb897a5f1','8b18e2b9-3aa1-4d38-8c14-59732772d196'," \
                   "'8b7f9032-884d-49e0-8b03-1f6cdb9a1fab','8da8c740-7630-452a-84cf-542b6e7b4721'," \
                   "'8e35c5ec-f639-43b5-a766-40f39e681836','8f8c65ef-456b-4695-b743-b8d687616777'," \
                   "'9b31fdbb-c497-4e89-955a-684af7799f73','9f68518a-cddc-4087-928b-4812fe523839'," \
                   "'a2234852-10b3-4fe7-bf1b-dbd9539c5bd4','a4860b73-d6f6-4c2f-b819-bd7543e26b84'," \
                   "'acc2d78e-f274-4495-bac9-8eba6a9ddca1','af30b5c5-f7b8-4209-b542-f2d4f62e68e9'," \
                   "'b8f29eb6-0416-4c43-a524-9031e2be7c95','bb58a326-69d5-4023-a5c6-51bed28d560c'," \
                   "'bc3f300d-5e94-41ae-91cf-780ad036ddfa','c2df839b-2fbb-4fd8-b88e-962cd7151469'," \
                   "'c713d05c-6ae2-430f-875f-7da1ae094aa7','cccddddb-8679-49be-8ad2-e5e8cd5ada97'," \
                   "'d0437592-0479-49c8-b98b-2b143cdbb5ed','d55bae2e-2a95-4f7c-af64-82d139728caf'," \
                   "'d674f42c-ddbf-483d-8827-b785079c0171','efb30b17-65b5-467b-827b-4f92ce2b19ec'," \
                   "'f7326043-f1c7-4f2e-94a8-cf9851dd686a','7f9e5272-4531-4ded-9bfd-4f8910f704d0'," \
                   "'44ecc31d-e9e4-4622-b56d-1926d20c858b','26ae748e-c681-43c5-852a-e4829e8b13df'," \
                   "'67759460-8eef-4b6f-9e4d-c90d0a5bc0f0','00deeeec-cd0f-4250-a267-f9b2c884d28b'," \
                   "'10e0b555-01f1-4b92-b3ab-379c3b04d1b3','14ed9f66-d559-445e-b1f3-7e9217e7ad4c'," \
                   "'167d1f0d-655e-4ad5-9369-5e6251f128b1','1c5737fe-d500-4026-8109-691291d19998'," \
                   "'1cc2f3e8-8793-490c-a084-5e2892760ff1','1de1b93c-8d1b-41bf-8a04-67253b99ec9f'," \
                   "'1fa4b0ef-dab9-4571-867d-67c6e97d2ca3','2180e33a-38b2-40fe-92f1-da72318474ea'," \
                   "'2b35190b-2680-45b9-9fe4-da83828350e1','2e78582b-c965-442f-939e-3dad63e56d51'," \
                   "'34572267-6ef4-4f9f-8016-15ddd7141b45','361ebc4b-f406-4f18-a465-fc846308f39a'," \
                   "'394baefe-9af2-4378-a0eb-4efcc28e75be','39888c12-a074-4477-93fc-33656d4bc4f8'," \
                   "'3a22481d-8394-4175-b4ee-b995ea6d862f','3c7aac82-8c83-4723-89fa-8ce4a08f4cc3'," \
                   "'4049f41e-820f-46a4-abc7-b58f7de2b701','413aed50-e00b-4e23-b6a8-14756748d7ec'," \
                   "'43ab717a-48f9-4780-a004-c4e5f6b95679','46fbf66e-e84b-4c9e-85e7-5f73dd289167'," \
                   "'4dc62fb5-c710-44ce-915a-4f3563b25ce9','53df2b07-dc4c-4727-a871-ebabda63ad88'," \
                   "'575060a3-f959-4f6e-b565-83308be939ad','588e3691-7dd2-4786-8277-76178981e8da'," \
                   "'5ce7f4aa-556e-491d-80d1-427caf9d0761','5e09e3b4-b1ae-4ebe-8267-40e2cf8592a3'," \
                   "'65e42b89-16e7-493e-a85b-f72348cb5d64','66082ff7-7d32-496a-bd7c-cb96e436d3aa'," \
                   "'66879807-04cc-47ac-a758-e655bdbb09d9','6775eab8-2c7f-4c82-94f3-03c351e0ef01'," \
                   "'6a67f586-17ec-4576-bf40-81fb27f50b32','6ae4be30-b760-440f-be31-b88ea7046352'," \
                   "'7034e8e1-4e40-414d-a3b6-c4b039ad46ae','7204e4cf-fd62-4937-bed8-526c51e6f82a'," \
                   "'77eafa4b-7dd4-43db-9fc2-5df055d4668c','78040adc-5d50-46d8-a565-5c8cff8f77c9'," \
                   "'90c43b5a-2c44-471c-83bc-306297d0f171','9a3fb712-8a95-4a2f-bc1c-cc356b1a5c41'," \
                   "'a7f80e88-4ada-4d50-be81-7198c754d1f8','adaf2c09-2a82-43ce-83b3-8e67d3235fa7'," \
                   "'afb00c08-e357-4136-8b07-cdc1d170323f','b50d787f-e2d3-43c6-8575-3e236ae0679e'," \
                   "'b87a1cc9-38c5-4def-b51e-9d7e1b100822','c00a22e5-80a8-4a0b-a6e2-a9166bf7af80'," \
                   "'c4198b57-d0b7-44f0-b6ce-e6b02bb40878','c81f244d-d5c6-4b32-983d-1803f44fbbe8'," \
                   "'ce75d5a3-5728-4eca-87c0-1a61a22a970f','d4604331-1311-425a-a503-817b25c85c9c'"

TOP10_customers = "'0adc5613-277f-4e3e-b0f4-6fefe122c5d3','103cedbc-262b-4c5e-94e0-f5d35c0a9c9d'," \
                   "'1672e2ed-5d4c-4b40-a29c-5ca85f44c53a','17cb7f39-3cfc-49ac-a2a4-6b91422ebe22'," \
                   "'1fa8c0d3-9909-4eda-b6d8-26e3de126629','24288b84-d9ca-499a-b1ca-5e65a4f42edf'," \
                   "'24eb6f9b-5f4f-4b57-be99-28c2403d1f86','255ec9ba-0fa0-4862-83fc-ac328f0e6260'," \
                   "'29326cd0-bf41-4c32-8893-d87bad71e065','33aeda48-f68c-4a04-8afd-d70185372a9e'"

TOP = "10"


def get_dataset(type):
    """
    Get benchmarking_customer_metrics dataset and format for visualisation
    """

    if type == 1:
        return get_dataset1()
    else:
        return get_dataset2()


def get_dataset1():
    kwargs = {"encoding": "utf-8"}
    ds_con = Extract(project_name="benchmarking", con_name="data_science", con_type="postgres")

    cust_metric_query = """
        select *
        from public.benchmarking_customer_metrics
        """
    cust_metric_df = ds_con.execute(save_file='all_benchmarking_customer_data.csv', query=cust_metric_query,
                                    **kwargs)
    cust_metric_df['windowstart'] = cust_metric_df['windowstart'] \
        .apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    cust_metric_df['httptrafficamount'] = cust_metric_df['httptrafficamount'].apply(lambda x: bytes_to_megabytes(x))
    cust_metric_df['httpstrafficamount'] = cust_metric_df['httpstrafficamount'].apply(
        lambda x: bytes_to_megabytes(x))

    device_metric_query = """
    select *
    from public.benchmarking_device_metrics
    """
    device_metric_df = ds_con.execute(save_file='all_benchmarking_device_data.csv', query=device_metric_query,
                                      **kwargs)
    device_metric_df['windowstart'] = device_metric_df['windowstart'] \
        .apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S'))
    device_metric_df['datausage'] = device_metric_df['datausage'].apply(lambda x: bytes_to_megabytes(x))
    device_metric_df['roamingdataamount'] = device_metric_df['roamingdataamount'] \
        .apply(lambda x: bytes_to_megabytes(x))

    return cust_metric_df, device_metric_df


def get_dataset2():
    kwargs = {"encoding": "utf-8"}
    rs_con = Extract(project_name="benchmarking", con_name="redshift_secondary", con_type="postgres")

    if os.path.exists('../../data/Dataset2_custmers_TOP' + TOP + '.csv'):
        print('customer file exists...')
        cust_metric_df = pd.read_csv('../../data/Dataset2_custmers_TOP' + TOP + '.csv')

        cust_metric_df['windowstart'] = cust_metric_df['windowstart'].apply(lambda x: ms_sice_epoch_to_datetime(x))
    else:
        cust_metric_query = """
            select *
            from public.bm_customer_metrics
            where customerid in ({})
            order by windowstart asc
            """.format(TOP10_customers)
        cust_metric_df = rs_con.execute(save_file='all_benchmarking_customer_data_dataset2_TOP' + TOP + '.csv',
                                        query=cust_metric_query,
                                        **kwargs)
        cust_metric_df['windowstart'] = cust_metric_df['windowstart'].apply(lambda x: ms_sice_epoch_to_datetime(x))
        cust_metric_df['nonweb_traffic_amount'] = cust_metric_df['nonweb_traffic_amount']\
            .apply(lambda x: bytes_to_megabytes(x))
        cust_metric_df['web_traffic_amount'] = cust_metric_df['web_traffic_amount']\
            .apply(lambda x: bytes_to_megabytes(x))

    cust_metric_df['caphitcount'] = cust_metric_df['cap_hit_cnt']

    if os.path.exists('../../data/Dataset2_devices_TOP' + TOP + '.csv'):
        print('device file exists...')
        device_metric_df = pd.read_csv('../../data/Dataset2_devices_TOP' + TOP + '.csv')

        device_metric_df['windowstart'] = device_metric_df['windowstart'].apply(lambda x: ms_sice_epoch_to_datetime(x))

    else:
        device_metric_query = """
            select *
            from public.bm_device_metrics
            where customerid in ({})
            order by windowstart asc
            """.format(TOP10_customers)
        device_metric_df = rs_con.execute(save_file='all_benchmarking_device_data_dataset2_TOP' + TOP + '.csv',
                                          query=device_metric_query,
                                          **kwargs)
        device_metric_df['datausage'] = device_metric_df['datausage'].apply(lambda x: bytes_to_megabytes(x))
        device_metric_df['roaming_data_amount'] = device_metric_df['roaming_data_amount'] \
            .apply(lambda x: bytes_to_megabytes(x))
        device_metric_df['https_traffic_amount'] = device_metric_df['https_traffic_amount']\
            .apply(lambda x: bytes_to_megabytes(x))
        device_metric_df['http_traffic_amount'] = device_metric_df['http_traffic_amount'] \
            .apply(lambda x: bytes_to_megabytes(x))

        device_metric_df.to_csv('../../data/Dataset2_devices_TOP' + TOP + '.csv')

        device_metric_df['windowstart'] = device_metric_df['windowstart'].apply(lambda x: ms_sice_epoch_to_datetime(x))

    device_metric_df['https_http_ratio'] = device_metric_df\
        .apply(lambda row: ratio(row['https_traffic_amount'], row['http_traffic_amount']), axis=1)

    return cust_metric_df, device_metric_df


def ratio(a, b):
    """
    ratio = a/b
    """
    if b == 0:
        if a > 0:
            return 10000 # Arbitary large for this example
        return 1
    else:
        return a/b


def ms_sice_epoch_to_datetime(x):
    dt = datetime.datetime.fromtimestamp(x / 1000.0).strftime('%Y-%m-%d %H:%M:%S')
    return datetime.datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')


def bytes_to_megabytes(bytes):
    """
    Helper for converting bytes to megabytes. We shall use the 'old' notation: 1 Megabyte = 1024^2 bytes
    """
    return bytes / (1024 ** 2)


class Data:
    def __init__(self, type=1):
        self.cust_metric_df, self.device_metric_df = get_dataset(type)
        if type == 1:
            self.cust_stats = ['httptrafficamount', 'httpstrafficamount', 'caphitcnt']
            self.device_stats = ['datausage', 'datausage,perdevice']
        else:
            self.cust_stats = ['nonweb_traffic_amount', 'web_traffic_amount', 'caphitcount']
            self.device_stats = ['datausage','roaming_data_amount','cap_hit_cnt', 'policy_block_hit_cnt',
                                 'http_traffic_amount', 'https_traffic_amount', 'app_cnt', 'site_cnt']

    def df_choice(self, stat):
        if stat in self.cust_stats:
            return self.cust_metric_df
        else:
            return self.device_metric_df