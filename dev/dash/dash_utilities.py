from __future__ import division
from bokeh.colors import RGB
import pandas as pd

Wandera_Green = RGB(r=54,g=161, b=56)
Wandera_Green_Hex = '#36A138'


def average_per_timeframe(df, col, id, timeframe):
    df2 = df.filter(['windowstart', col], axis=1)
    df_timeidx = df2.set_index('windowstart')
    df_avepertime = df_timeidx.resample(timeframe).mean().reset_index()
    #df_avepertime = df_timeidx.groupby(df_timeidx.index.hour).mean().reset_index(drop=True)
    df_avepertime['id'] = id

    return df_avepertime[[col, 'id', 'windowstart']]


def percentage_per_timeframe(df, yaxis, id, timeframe):
    df2 = df.filter(['windowstart', yaxis, 'deviceid'], axis=1)
    grouper = df2.groupby([pd.Grouper(key='windowstart', freq=timeframe), 'deviceid'])[yaxis].sum().reset_index()
    l_d = []
    num_devices = len(df.deviceid.unique())
    for date in grouper['windowstart'].unique():
        frame = grouper[grouper['windowstart'] == pd.Timestamp(date)]
        l_d.append({'windowstart': date, yaxis: 100 * len(frame[frame[yaxis] > 0]) / num_devices, 'id': id})
    return pd.DataFrame(l_d)[[yaxis, 'id', 'windowstart']]


def compare_times(df1, df2, timeframe):
    if set(df1['windowstart'].unique()) == set(df2['windowstart'].unique()):
        return df1, df2
    else:
        all_times = pd.date_range(min(min(df1['windowstart']), min(df2['windowstart'])),
                                  max(max(df1['windowstart']),max(df2['windowstart'])),
                                  freq=timeframe)
        df1_datestrings = [pd.to_datetime(str(dt)).strftime('%Y-%m-%d %H:%M:%S') for dt in df1['windowstart'].unique()]
        df2_datestrings = [pd.to_datetime(str(dt)).strftime('%Y-%m-%d %H:%M:%S') for dt in df2['windowstart'].unique()]
        for t in all_times:
            ts = pd.to_datetime(str(t))
            t_string = ts.strftime('%Y-%m-%d %H:%M:%S')
            if t_string not in df1_datestrings:
                if len(df1.columns == 3):
                    df_1 = pd.DataFrame([[0, df1.iloc[0]['id'], t]], columns=df1.columns)
                else:
                    df_1 = pd.DataFrame([[t, 0, 0, 0, 0, 0, 0, df1.iloc[0]['id']]], columns=df1.columns)
                df1 = df1.append(df_1)
            if t_string not in df2_datestrings:
                if len(df1.columns == 3):
                    df_2 = pd.DataFrame([[0, df2.iloc[0]['id'], t]], columns=df2.columns)
                else:
                    df_2 = pd.DataFrame([[t, 0, 0, 0, 0, 0, 0, df2.iloc[0]['id']]], columns=df2.columns)
                df2 = df2.append(df_2)
        df1_edit = df1.sort_values('windowstart').reset_index(drop=True)
        df2_edit = df2.sort_values('windowstart').reset_index(drop=True)
        return df1_edit, df2_edit
