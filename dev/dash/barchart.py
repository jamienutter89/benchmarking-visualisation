from __future__ import division
from bokeh.models import HoverTool, ColumnDataSource, FactorRange
from bokeh.plotting import figure
import pandas as pd
from dash_utilities import average_per_timeframe, Wandera_Green, compare_times, percentage_per_timeframe


class BarChart:
    def __init__(self, data, id, statistic, x_axis, width=0.5, title="", x_axis_label="", y_axis_label="",
                 color=Wandera_Green, dropdown=None, data_dropdown=None):
        self.data = data
        self.id = id
        self.x_axis = x_axis
        self.statistic = statistic
        self.y_axis = statistic.split(',')[0]
        self.width = width
        self.title = title
        self.x_axis_label = x_axis_label
        self.y_axis_label = y_axis_label
        self.color = color
        self.dropdown = dropdown
        self.data_dropdown = data_dropdown

        self.src, self.xrange = self.make_dataset()
        self.plot = self.make_plot()

        if self.dropdown is not None:
            self.dropdown.on_change("value", self.dropdown_callback)

        if self.data_dropdown is not None:
            self.data_dropdown.on_change("value", self.data_dropdown_callback)

    def make_dataset(self):
        df = self.data.df_choice(self.statistic)
        # Split by this customer and then the rest
        cust = df[df['customerid'] == self.id].reset_index(drop=True)
        others = df[df['customerid'] != self.id].reset_index(drop=True)

        if '%device' in self.statistic:
            gb1 = cust.groupby(['deviceid']).sum().reset_index()
            gb2 = others.groupby(['deviceid']).sum().reset_index()

            num_caphit1 = len(gb1[gb1.capreached > 0])
            num_caphit2 = len(gb2[gb2.capreached > 0])

            perc1 = 100 * num_caphit1 / len(gb1)
            perc2 = 100 * num_caphit2 / len(gb2)

            return ColumnDataSource(data=dict(x1=['Customer', 'Other Customers'], y1=[perc1, perc2])), \
                   ['Customer', 'Other Customers']
        else:
            # Total Averages
            cust_ave = pd.DataFrame(cust.mean().to_dict(), index=[cust.index.values[-1]], columns=cust.columns)\
                .reset_index(drop=True)
            others_ave = pd.DataFrame(others.mean().to_dict(), index=[others.index.values[-1]], columns=others.columns)\
                .reset_index(drop=True)

            # TOTALS
            #cust_ave = pd.DataFrame(cust.sum().to_dict(), index=[cust.index.values[-1]], columns=cust.columns)\
            #    .reset_index(drop=True)
            #others_ave = pd.DataFrame(others.sum().to_dict(), index=[others.index.values[-1]], columns=others.columns)\
            #    .reset_index(drop=True)

            cust_ave['id'] = 'Customer'
            others_ave['id'] = 'Other Customers'
            if 'perdevice' in self.statistic:
                numberdevices_cust = len(cust['deviceid'].unique())
                cust_ave[self.y_axis] = cust_ave[self.y_axis].apply(lambda x: x / numberdevices_cust)
                numberdevices_others = len(others['deviceid'].unique())
                others_ave[self.y_axis] = others_ave[self.y_axis].apply(lambda x: x / numberdevices_others)
            data = pd.concat([cust_ave, others_ave])
            return ColumnDataSource(data=dict(x1=data[self.x_axis], y1=data[self.y_axis])), list(data[self.x_axis])

    def make_plot(self):
        p = figure(plot_height=600, plot_width=600,
                   x_range=self.xrange,
                   title=self.title,
                   x_axis_label=self.x_axis_label,
                   y_axis_label=self.y_axis_label,
                   tools=[], toolbar_location=None
                   )

        p.vbar(source=self.src, x='x1', top='y1', width=self.width, bottom=0, color=self.color, line_color="#033649")

        p.y_range.start = 0
        p.xgrid.grid_line_color = None

        hover = HoverTool()
        hover.tooltips = [('Value', "@y1")]
        p.add_tools(hover)

        return p

    def dropdown_callback(self, attr, old, new):
        self.y_axis = self.dropdown.value.split(',')[0]
        self.statistic = self.dropdown.value

        if '%device' in self.statistic:
            self.plot.title.text = "Percentage Compare"
        else:
            self.plot.title.text = 'Average Compare'

        new_src, x_range = self.make_dataset()
        self.plot.yaxis.axis_label = self.dropdown.value
        self.src.data.update(new_src.data)

    def data_dropdown_callback(self, attr, old, new):
        self.id = self.data_dropdown.value
        new_src, x_range = self.make_dataset()
        self.src.data.update(new_src.data)

    def get(self):
        return self.plot


class NestedBarChart:
    def __init__(self, data, id, statistic, x_1, x_2, width=1, title="", x_axis_label="", y_axis_label="",
                 timeframe="H", color=Wandera_Green, dropdown=None, data_dropdown=None, timeframe_dropdown=None,
                 device_dropdown=None):
        self.data = data
        self.id = id
        self.x_1 = x_1
        self.x_2 = x_2
        self.statistic = statistic
        self.y_axis = statistic.split(',')[0]
        self.width = width
        self.title = title
        self.x_axis_label = x_axis_label
        self.y_axis_label = y_axis_label
        self.color = color
        self.dropdown = dropdown
        self.data_dropdown = data_dropdown
        self.timeframe_dropdown = timeframe_dropdown
        self.timeframe = timeframe
        self.device_dropdown = device_dropdown.dropdown
        self.device = self.device_dropdown.value

        self.src, self.x_range = self.make_dataset()
        self.plot = self.make_plot()

        if self.device_dropdown is not None:
            self.device_dropdown.on_change("value", self.device_dropdown_callback)

        if self.timeframe_dropdown is not None:
            self.timeframe_dropdown.on_change("value", self.timeframe_dropdown_callback)

        if self.dropdown is not None:
            self.dropdown.on_change("value", self.dropdown_callback)

        if self.data_dropdown is not None:
            self.data_dropdown.on_change("value", self.data_dropdown_callback)

    def make_dataset(self):
        df = self.data.df_choice(self.statistic)

        if 'justdevice-customer' in self.statistic:
            temp = df[df['customerid'] == self.id]
            data1 = temp[temp['deviceid'] == self.device].reset_index(drop=True)
            label = 'Chosen Device'
            data2 = temp[temp['deviceid'] != self.device].reset_index(drop=True)
            label2 = 'Other Devices Average Within Customer'
        elif 'justdevice' in self.statistic:
            data1 = df[df['deviceid'] == self.device].reset_index(drop=True)
            label = 'Chosen Device'
            data2 = df[df['deviceid'] != self.device].reset_index(drop=True)
            label2 = 'Other Devices'
        else:
            data1 = df[df['customerid'] == self.id].reset_index(drop=True)
            label = 'Customer Traffic'
            data2 = df[df['customerid'] != self.id].reset_index(drop=True)
            label2 = 'Other Customer Traffic'

        if '%device' in self.statistic:
            data1_perc = percentage_per_timeframe(data1, self.y_axis, id=label, timeframe=self.timeframe)
            data2_perc = percentage_per_timeframe(data2, self.y_axis, id=label2, timeframe=self.timeframe)
            data1_stat, data2_stat = compare_times(data1_perc, data2_perc, timeframe=self.timeframe)
        else:
            #  Average
            data1_stat = average_per_timeframe(data1, col=self.y_axis, id=label, timeframe=self.timeframe)
            data2_stat = average_per_timeframe(data2, col=self.y_axis, id=label2, timeframe=self.timeframe)
            if 'perdevice' in self.statistic:
                numberdevices1 = len(data1['deviceid'].unique())
                data1_stat[self.y_axis] = data1_stat[self.y_axis].apply(lambda x: x / numberdevices1)
                numberdevices2 = len(data2['deviceid'].unique())
                data2_stat[self.y_axis] = data2_stat[self.y_axis].apply(lambda x: x / numberdevices2)
                data1_stat, data2_stat = compare_times(data1_stat, data2_stat, timeframe=self.timeframe)
        averages = pd.concat([data1_stat, data2_stat]).reset_index()
        averages = averages.fillna(0)
        x = [(str(x1), str(x2)) for x1 in averages[self.x_1].unique() for x2 in averages[self.x_2].unique()]
        counts = sum(zip(averages[self.y_axis]), ())
        return ColumnDataSource(data=dict(x=x, counts=counts)), x

    def make_plot(self):
        p = figure(x_range=FactorRange(*self.x_range),
                   title=self.title,
                   x_axis_label=self.x_axis_label,
                   y_axis_label=self.y_axis_label,
                   tools="", toolbar_location=None)

        p.vbar(x='x', top='counts', width=0.9 * self.width, source=self.src, color=self.color, line_color="#033649")

        p.y_range.start = 0
        p.x_range.range_padding = 0.1 * self.width
        p.xaxis.major_label_orientation = 1
        p.xgrid.grid_line_color = None

        hover = HoverTool()
        hover.tooltips = [('Value', "@counts")]
        p.add_tools(hover)

        return p

    def dropdown_callback(self, attr, old, new):
        self.y_axis = self.dropdown.value.split(',')[0]
        self.statistic = self.dropdown.value

        if '%device' in self.statistic:
            self.plot.title.text = "Percentage Compare"
        else:
            self.plot.title.text = 'Average Compare'

        new_src, self.x_range = self.make_dataset()
        self.plot.yaxis.axis_label = self.dropdown.value
        self.plot.x_range.factors = self.x_range
        self.src.data.update(new_src.data)

    def data_dropdown_callback(self, attr, old, new):

        if 'justdevice' in self.statistic:
            self.device = self.device_dropdown.value

        self.id = self.data_dropdown.value
        new_src, self.x_range = self.make_dataset()
        self.plot.x_range.factors = self.x_range
        self.src.data.update(new_src.data)

    def timeframe_dropdown_callback(self, attr, old, new):
        if self.timeframe_dropdown.value == 'Hour':
            self.timeframe = 'H'
        elif self.timeframe_dropdown.value == 'Half Hour':
            self.timeframe = '30T'
        elif self.timeframe_dropdown.value == 'Day':
            self.timeframe = 'D'
        elif self.timeframe_dropdown.value == 'Week':
            self.timeframe = '7D'
        elif self.timeframe_dropdown.value == 'Month':
            self.timeframe = '30D'
        new_src, self.x_range = self.make_dataset()
        self.plot.x_range.factors = self.x_range
        self.src.data.update(new_src.data)

    def device_dropdown_callback(self, attr, old, new):
        self.device = self.device_dropdown.value
        new_src, self.x_range = self.make_dataset()
        self.src.data.update(new_src.data)


    def get(self):
        return self.plot
