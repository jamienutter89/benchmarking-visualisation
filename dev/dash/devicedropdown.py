from bokeh.models.widgets import Select


class DeviceDropdown:
    def __init__(self, data, initialcustomer, customer_dropdown):
        self.data = data
        self.customer=initialcustomer
        self.customer_dropdown = customer_dropdown

        self.customer_dropdown.on_change("value", self.customer_dropdown_callback)

        device_list = self.data[self.data['customerid'] == self.customer]['deviceid'].unique().tolist()
        self.dropdown = Select(value=device_list[0], title='Customer Devices:', options=device_list)

    def update(self):
        device_list = self.data[self.data['customerid'] == self.customer]['deviceid'].unique().tolist()
        self.dropdown.options = device_list
        self.dropdown.value = device_list[0]

    def customer_dropdown_callback(self, attr, old, new):
        self.customer = self.customer_dropdown.value
        self.update()
