from histogram import Histogram
from barchart import BarChart, NestedBarChart
from linechart import LineChart
from kde import KDE
from devicedropdown import DeviceDropdown
from textbox import TextBox

from bokeh.io import curdoc
from bokeh.models import WidgetBox
from bokeh.layouts import column, row, layout


class Plots:
    def __init__(self, data, initial_y, initial_customerid, initial_timeframe, dropdown, customer_dropdown, stat_dropdown,
                 timeframe_dropdown):
        self.data = data
        self.y = initial_y
        self.customerid = initial_customerid
        self.dropdown = dropdown
        self.customer_dropdown = customer_dropdown
        self.stat_dropdown = stat_dropdown
        self.timeframe_dropdown = timeframe_dropdown
        self.timeframe = initial_timeframe

        self.device_dropdown = DeviceDropdown(self.data.device_metric_df, self.customerid, self.customer_dropdown)

        self.plot = LineChart(self.data, id=self.customerid, statistic=self.y,
                              x_axis='index', title='Per Hour Average Compare',
                              comparison=True,
                              y_axis_label=self.y,
                              timeframe=self.timeframe,
                              dropdown=self.stat_dropdown,
                              data_dropdown=self.customer_dropdown,
                              timeframe_dropdown=self.timeframe_dropdown,
                              device_dropdown=self.device_dropdown)

        self.dashboard = layout([column([row([WidgetBox(self.customer_dropdown), WidgetBox(self.stat_dropdown)]),
                                         row([WidgetBox(self.dropdown), WidgetBox(self.timeframe_dropdown)])]),
                                 [self.plot.get()]
                                 ], sizing_mode='stretch_both')

        self.dropdown.on_change("value", self.dropdown_callback)
        self.stat_dropdown.on_change("value", self.stat_dropdown_callback)
        self.customer_dropdown.on_change("value", self.customer_dropdown_callback)
        self.timeframe_dropdown.on_change("value", self.timeframe_dropdown_callback)

        curdoc().add_root(self.dashboard)
        curdoc().title = "dash"

    def timeframe_dropdown_callback(self, attr, old, new):
        if self.timeframe_dropdown.value == 'Hour':
            self.timeframe = 'H'
        elif self.timeframe_dropdown.value == 'Half Hour':
            self.timeframe = '30T'
        elif self.timeframe_dropdown.value == 'Day':
            self.timeframe = 'D'
        elif self.timeframe_dropdown.value == 'Week':
            self.timeframe = '7D'
        elif self.timeframe_dropdown.value == 'Month':
            self.timeframe = '30D'
        self.update()

    def customer_dropdown_callback(self, attr, old, new):
        self.customerid = self.customer_dropdown.value
        self.update()

    def stat_dropdown_callback(self, attr, old, new):
        self.y = self.stat_dropdown.value
        self.update()

    def dropdown_callback(self, attr, old, new):
        print(self.dropdown.value)
        if self.dropdown.value == 'Line Chart':
            self.plot = LineChart(self.data, id=self.customerid, statistic=self.y,
                                  x_axis='index', title='Average Compare',
                                  comparison=True,
                                  y_axis_label=self.y,
                                  timeframe=self.timeframe,
                                  dropdown=self.stat_dropdown,
                                  data_dropdown=self.customer_dropdown,
                                  timeframe_dropdown=self.timeframe_dropdown,
                                  device_dropdown=self.device_dropdown)
        elif self.dropdown.value == 'Nested Bar Chart':
            self.plot = NestedBarChart(self.data, id=self.customerid, statistic=self.y,
                                       x_1='id', x_2='index',
                                       title='Average Compare',
                                       y_axis_label=self.y,
                                       timeframe=self.timeframe,
                                       dropdown=self.stat_dropdown,
                                       data_dropdown=self.customer_dropdown,
                                       timeframe_dropdown=self.timeframe_dropdown,
                                       device_dropdown=self.device_dropdown)
        elif self.dropdown.value == 'Bar Chart':
            self.plot = BarChart(self.data, id=self.customerid, statistic=self.y,
                                 x_axis='id', title='Average Compare',
                                 y_axis_label=self.y,
                                 dropdown=self.stat_dropdown,
                                 data_dropdown=self.customer_dropdown)
        elif self.dropdown.value == 'Histogram':
            self.plot = Histogram(self.data, self.customerid, statistic=self.y,
                                  y_axis_label='count',
                                  x_axis_label=self.y,
                                  bins='auto',
                                  density=False,
                                  timeframe=self.timeframe,
                                  dropdown=self.stat_dropdown,
                                  data_dropdown=self.customer_dropdown,
                                  timeframe_dropdown=self.timeframe_dropdown,
                                  device_dropdown=self.device_dropdown)
        elif self.dropdown.value == 'Histogram-Density':
            self.plot = Histogram(self.data, self.customerid, statistic=self.y,
                                  x_axis_label=self.y,
                                  y_axis_label='Probability Density',
                                  bins='auto',
                                  density=True,
                                  timeframe=self.timeframe,
                                  dropdown=self.stat_dropdown,
                                  data_dropdown=self.customer_dropdown,
                                  timeframe_dropdown=self.timeframe_dropdown,
                                  device_dropdown=self.device_dropdown)
        elif self.dropdown.value == 'Kernel Density Estimation':
            self.plot = KDE(self.data, id=self.customerid, statistic=self.y,
                            x_axis_label=self.y,
                            y_axis_label='KDE',
                            stat_dropdown=self.stat_dropdown,
                            timeframe=self.timeframe,
                            data_dropdown=self.customer_dropdown,
                            timeframe_dropdown=self.timeframe_dropdown,
                            device_dropdown=self.device_dropdown)
        self.update()

    def update(self):

        if 'justdevice' in self.stat_dropdown.value:
            self.dashboard.children = layout([column([row([WidgetBox(self.customer_dropdown), WidgetBox(self.stat_dropdown)]),
                                         row([WidgetBox(self.dropdown), WidgetBox(self.timeframe_dropdown)]),
                                         row([WidgetBox(self.device_dropdown.dropdown)])]),
                                          [self.plot.get()]
                                          ], sizing_mode='stretch_both').children
        else:
            self.dashboard.children = layout(
                [column([row([WidgetBox(self.customer_dropdown), WidgetBox(self.stat_dropdown)]),
                         row([WidgetBox(self.dropdown), WidgetBox(self.timeframe_dropdown)])]),
                 [self.plot.get()]
                 ], sizing_mode='stretch_both').children
