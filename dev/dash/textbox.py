from bokeh.layouts import widgetbox
from bokeh.models.widgets import Div


class TextBox:
    def __init__(self, text, font_size_str='200', color='darkslategray'):
        self.text = text
        self.font_size_str = font_size_str
        self.color = color

        self.box = self.make_textbox()

    def make_textbox(self):
        p = Div(text=self.text, style={'font-size': self.font_size_str + '%', 'text_font': 'helvetica',
                                       'color': self.color})
        return widgetbox(p)

    def get(self):
        return self.box
