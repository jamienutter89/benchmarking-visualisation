from sklearn.neighbors import KernelDensity
import numpy as np
from dash_utilities import Wandera_Green, average_per_timeframe
from bokeh.plotting import figure
from bokeh.models import HoverTool, ColumnDataSource


def kde(x, x_grid, bandwidth=0.2, **kwargs):
    """
    Kernel Density Estimation with Scikit-learn
    """
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)


class KDE:
    def __init__(self, data, id, statistic,
                 x_axis_label='', y_axis_label='', bins=10, timeframe='H',
                 color=Wandera_Green, stat_dropdown=None, data_dropdown=None, timeframe_dropdown=None,
                 device_dropdown=None):
        self.data = data
        self.id = id
        self.statistic = statistic
        self.y_axis = statistic.split(',')[0]
        self.bins = bins
        self.color = color
        self.stat_dropdown = stat_dropdown
        self.data_dropdown = data_dropdown
        self.y_axis_label = y_axis_label
        self.x_axis_label = x_axis_label
        self.timeframe_dropdown = timeframe_dropdown
        self.timeframe = timeframe
        self.device_dropdown = device_dropdown.dropdown
        self.device = self.device_dropdown.value
        self.bandwidth = 0.2

        self.src = self.make_dataset()
        self.plot = self.make_plot()

        if self.device_dropdown is not None:
            self.device_dropdown.on_change("value", self.device_dropdown_callback)

        if self.timeframe_dropdown is not None:
            self.timeframe_dropdown.on_change("value", self.timeframe_dropdown_callback)

        if self.stat_dropdown is not None:
            self.stat_dropdown.on_change("value", self.stat_dropdown_callback)

        if self.data_dropdown is not None:
            self.data_dropdown.on_change("value", self.data_dropdown_callback)

    def make_dataset(self):
        if '%device' not in self.statistic:
            df = self.data.df_choice(self.statistic)

            if 'justdevice-customer' in self.statistic:
                temp = df[df['customerid'] == self.id]
                df1 = temp[temp['deviceid'] == self.device]
                label = 'Chosen Device'
            elif 'justdevice' in self.statistic:
                df1 = df[df['deviceid'] == self.device]
                label = 'Chosen Device'
            else:
                df1 = df[df['customerid'] == self.id]
                label = 'Customer Traffic'

            data1 = average_per_timeframe(df1, col=self.y_axis, id=label, timeframe=self.timeframe)
            data1 = data1.fillna(0)

            if 'justdevice-customer' in self.statistic:
                temp = df[df['customerid'] == self.id]
                df2 = temp[temp['deviceid'] != self.device]
                label2 = 'Other Devices Average Within Customer'
            elif 'justdevice' in self.statistic:
                df2 = df[df['deviceid'] != self.device]
                label2 = 'Other Devices'
            else:
                df2 = df[df['customerid'] != self.id]
                label2 = 'Other Customer Traffic'

            data2 = average_per_timeframe(df2, col=self.y_axis, id=label2, timeframe=self.timeframe)
            data2 = data2.fillna(0)

            if 'perdevice' in self.statistic:
                N1 = len(df1['deviceid'].unique())
                data1[self.y_axis] = data1[self.y_axis].apply(lambda x: x / N1)
                N2 = len(df2['deviceid'].unique())
                data2[self.y_axis] = data2[self.y_axis].apply(lambda x: x / N2)

            percent = 99
            percentile = np.percentile(df[self.y_axis], percent)
            x_range = np.linspace(0, percentile, 1000)
            kde1 = kde(data1[self.y_axis], x_range, bandwidth=self.bandwidth)
            kde2 = kde(data2[self.y_axis], x_range, bandwidth=self.bandwidth)
            data = {'x': x_range, 'y1': kde1, 'y2': kde2}
            source = ColumnDataSource(data)
        else:
            data = {'x': [0], 'y1': [0], 'y2': [0]}
            source = ColumnDataSource(data)

        return source

    def make_plot(self):
        p = figure(x_axis_label=self.x_axis_label,
                   y_axis_label=self.y_axis_label,
                   title="",
                   tools="box_zoom, reset",
                   toolbar_location="above")

        p.line('x', 'y1', source=self.src, line_color=self.color, legend="Customer Traffic",
               line_width=3, alpha=0.9)
        p.line('x', 'y2', source=self.src, line_color="#033649", legend="Other Customer Traffic",
               line_width=3, alpha=0.9)

        p.y_range.start = 0
        p.x_range.start = 0
        p.xgrid.grid_line_color = None
        p.legend.label_text_font_size = '8pt'
        p.legend.padding = 5
        p.legend.spacing = 1

        return p

    def stat_dropdown_callback(self, attr, old, new):
        self.y_axis = self.stat_dropdown.value.split(',')[0]
        self.statistic = self.stat_dropdown.value

        if '%device' in self.statistic:
            self.plot.title.text = "Histogram not suitable for this metric"
        else:
            self.plot.title.text = 'Average Compare'

        new_src = self.make_dataset()
        self.plot.xaxis.axis_label = self.stat_dropdown.value
        self.src.data.update(new_src.data)

    def data_dropdown_callback(self, attr, old, new):

        if 'justdevice' in self.statistic:
            self.device = self.device_dropdown.value

        self.id = self.data_dropdown.value
        new_src= self.make_dataset()
        self.src.data.update(new_src.data)

    def timeframe_dropdown_callback(self, attr, old, new):
        print(self.timeframe_dropdown.value)
        if self.timeframe_dropdown.value == 'Hour':
            self.timeframe = 'H'
        elif self.timeframe_dropdown.value == 'Half Hour':
            self.timeframe = '30T'
        elif self.timeframe_dropdown.value == 'Day':
            self.timeframe = 'D'
        elif self.timeframe_dropdown.value == 'Week':
            self.timeframe = '7D'
        elif self.timeframe_dropdown.value == 'Month':
            self.timeframe = '30D'
            print('UPDATING SRC')
        new_src = self.make_dataset()
        self.src.data.update(new_src.data)

    def device_dropdown_callback(self, attr, old, new):
        self.device = self.device_dropdown.value
        new_src = self.make_dataset()
        self.src.data.update(new_src.data)

    def get(self):
        return self.plot