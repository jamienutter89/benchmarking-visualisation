from __future__ import division
from plots import Plots
import numpy as np
from data import Data
from bokeh.models.widgets import Select


tools = 'pan'


def perc_change(new, old):
    """
    Helper for calculating percentage change
    """
    return 100*(new - old)/old



def get_dict_by_id(df, id):
    """
    Split a datatable into a dictionary by a given id field
    """
    ids = df[id].unique()
    d = {}
    for c in ids:
        d[c] = df[df[id] == c]
    return d


data = Data(type=2)
initial_y = 'web_traffic_amount'
initial_customerid = data.cust_metric_df.iloc[0]['customerid']
customer_list_combo = data.cust_metric_df['customerid'].unique().tolist()\
                      + data.device_metric_df['customerid'].unique().tolist()
customer_list = list(set(customer_list_combo))
customer_dropdown = Select(value=initial_customerid, title='Customer ID:', options=customer_list)

df = data.cust_metric_df

menu = [('web_traffic_amount', 'Customer Metric: Average Web Traffic Compare'),
        ('nonweb_traffic_amount', 'Customer Metric: Average Non-Web Traffic Compare'),
        ('caphitcount', 'Customer Metric: Average Cap Hit Count Compare'),
        ('datausage', 'Device Metric: Chosen Customer-Average Data Usage Compare'),
        ('roaming_data_amount', 'Device Metric: Chosen Customer-Average Roaming Data Compare'),
        ('cap_hit_cnt', 'Device Metric: Chosen Customer-Average Cap Hit Count Compare'),
        ('policy_block_hit_cnt', 'Device Metric: Chosen Customer-Average Policy Block Hit Compare'),
        ('http_traffic_amount', 'Device Metric: Chosen Customer-Average HTTP Traffic Compare'),
        ('https_traffic_amount', 'Device Metric: Chosen Customer-Average HTTPS Traffic Compare'),
        ('https_http_ratio', 'Device Metric: Chosen Customer-Average HTTPS to HTTP Ratio Compare'),
        ('app_cnt', 'Device Metric: Chosen Customer-Average App Count Compare'),
        ('site_cnt', 'Device Metric: Chosen Customer-Average Site Count Compare'),
        ('datausage,perdevice', 'Device Metric: Chosen Customer-Per Device Average Data Usage Compare'),
        ('roaming_data_amount,perdevice', 'Device Metric: Chosen Customer-Per Device Average Roaming Data Compare'),
        ('cap_hit_cnt,perdevice', 'Device Metric: Chosen Customer-Per Device Average Cap Hit Count Compare'),
        ('policy_block_hit_cnt,perdevice', 'Device Metric: Chosen Customer-Per Device Average Policy Block Hit Compare'),
        ('http_traffic_amount,perdevice', 'Device Metric: Chosen Customer-Per Device Average HTTP Traffic Compare'),
        ('https_traffic_amount,perdevice', 'Device Metric: Chosen Customer-Per Device Average HTTPS Traffic Compare'),
        ('https_http_ratio,perdevice', 'Device Metric: Chosen Customer-Per Device Average HTTPS to HTTP Ratio Compare'),
        ('app_cnt,perdevice', 'Device Metric: Chosen Customer-Per Device Average App Count Compare'),
        ('site_cnt,perdevice', 'Device Metric: Chosen Customer-Per Device Average Site Count Compare'),
        ('cap_hit_cnt,%device', 'Device Metric: Chosen Customer-Percentage of Devices Average Cap Hit Count Compare'),
        ('policy_block_hit_cnt,%device', 'Device Metric: Chosen Customer-Percentage of Devices Average Policy Block Hit Compare'),
        ('datausage,justdevice', 'Device Metric: Device Average Data Usage Compare'),
        ('roaming_data_amount,justdevice', 'Device Metric: Device Average Roaming Data Compare'),
        ('cap_hit_cnt,justdevice', 'Device Metric: Device Average Cap Hit Count Compare'),
        ('policy_block_hit_cnt,justdevice', 'Device Metric: Device Average Policy Block Hit Compare'),
        ('http_traffic_amount,justdevice', 'Device Metric: Device Average HTTP Traffic Compare'),
        ('https_traffic_amount,justdevice', 'Device Metric: Device Average HTTPS Traffic Compare'),
        ('https_http_ratio,justdevice', 'Device Metric: Device Average HTTPS to HTTP Ratio Compare'),
        ('app_cnt,justdevice', 'Device Metric: Device Average App Count Compare'),
        ('site_cnt,justdevice', 'Device Metric: Device Average Site Count Compare'),
        ('datausage,justdevice-customer', 'Device Metric: Device Average Data Usage Compare Within Customer'),
        ('roaming_data_amount,justdevice-customer', 'Device Metric: Device Average Roaming Data Compare Within Customer'),
        ('cap_hit_cnt,justdevice-customer', 'Device Metric: Device Average Cap Hit Count Compare Within Customer'),
        ('policy_block_hit_cnt,justdevice-customer', 'Device Metric: Device Average Policy Block Hit Compare Within Customer'),
        ('http_traffic_amount,justdevice-customer', 'Device Metric: Device Average HTTP Traffic Compare Within Customer'),
        ('https_traffic_amount,justdevice-customer', 'Device Metric: Device Average HTTPS Traffic Compare Within Customer'),
        ('https_http_ratio,justdevice-customer', 'Device Metric: Device Average HTTPS to HTTP Ratio Compare Within Customer'),
        ('app_cnt,justdevice-customer', 'Device Metric: Device Average App Count Compare Within Customer'),
        ('site_cnt,justdevice-customer', 'Device Metric: Device Average Site Count Compare Within Customer')]

dropdown = Select(value=initial_y, title='Statistic:', options=menu)

plot_dropdown = Select(value='Line Chart', title='Plot:', options=['Line Chart',
                                                                   'Nested Bar Chart',
                                                                   'Histogram',
                                                                   'Histogram-Density',
                                                                   'Kernel Density Estimation'])

timeframe_dropdown = Select(value='Day', title='Time-Frame Grouping:', options=['Half Hour', 'Hour', 'Day', 'Week', 'Month'])
initial_timeframe = 'D'

plot = Plots(data=data, initial_y=initial_y, initial_customerid=initial_customerid, initial_timeframe=initial_timeframe,
             dropdown=plot_dropdown,
             customer_dropdown=customer_dropdown,
             stat_dropdown=dropdown,
             timeframe_dropdown=timeframe_dropdown)



