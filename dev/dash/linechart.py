from bokeh.models import HoverTool, ColumnDataSource
from bokeh.plotting import figure
from dash_utilities import average_per_timeframe, Wandera_Green, percentage_per_timeframe


class LineChart:
    def __init__(self, data, id, statistic, x_axis, width=2, comparison=False,
                 title="", x_axis_label="", y_axis_label="", timeframe="H",
                 color=Wandera_Green, dropdown=None, data_dropdown=None, timeframe_dropdown=None, device_dropdown=None):
        self.data = data
        self.id = id
        self.x_axis = x_axis
        self.statistic = statistic
        self.y_axis = statistic.split(',')[0]
        self.width = width
        self.title = title
        self.x_axis_label = x_axis_label
        self.y_axis_label = y_axis_label
        self.color = color
        self.comparison = comparison
        self.dropdown = dropdown
        self.data_dropdown = data_dropdown
        self.timeframe_dropdown = timeframe_dropdown
        self.timeframe = timeframe
        self.device_dropdown = device_dropdown.dropdown
        self.device = self.device_dropdown.value

        self.src, self.src2 = self.make_dataset()
        self.plot = self.make_plot()

        if self.device_dropdown is not None:
            self.device_dropdown.on_change("value", self.device_dropdown_callback)

        if self.timeframe_dropdown is not None:
            self.timeframe_dropdown.on_change("value", self.timeframe_dropdown_callback)

        if self.dropdown is not None:
            self.dropdown.on_change("value", self.dropdown_callback)

        if self.data_dropdown is not None:
            self.data_dropdown.on_change("value", self.data_dropdown_callback)

    def make_dataset(self):
        df = self.data.df_choice(self.statistic)

        if 'justdevice-customer' in self.statistic:
            temp = df[df['customerid'] == self.id]
            data1 = temp[temp['deviceid'] == self.device]
            label = 'Chosen Device'
        elif 'justdevice' in self.statistic:
            data1 = df[df['deviceid'] == self.device]
            label = 'Chosen Device'
        else:
            data1 = df[df['customerid'] == self.id]
            label = 'Customer Traffic'

        if '%device' in self.statistic:
            data1_stat = percentage_per_timeframe(data1, self.y_axis, id=label, timeframe=self.timeframe)
        else:
            data1_stat = average_per_timeframe(data1, col=self.y_axis, id=label, timeframe=self.timeframe)
            if 'perdevice' in self.statistic:
                numberdevices = len(data1['deviceid'].unique())
                data1_stat[self.y_axis] = data1_stat[self.y_axis].apply(lambda x: x / numberdevices)
        data1_stat['color'] = self.color
        data1_stat = data1_stat.fillna(0)
        data1_ = data1_stat.reset_index()
        src = ColumnDataSource(data=dict(x=data1_[self.x_axis], y=data1_[self.y_axis],
                                         color=data1_['color'], name=data1_['id']))

        if self.comparison:

            if 'justdevice-customer' in self.statistic:
                temp = df[df['customerid'] == self.id]
                data2 = temp[temp['deviceid'] != self.device]
                label2 = 'Other Devices Average Within Customer'
            elif 'justdevice' in self.statistic:
                data2 = df[df['deviceid'] != self.device]
                label2 = 'Other Devices Average'
            else:
                data2 = df[df['customerid'] != self.id]
                label2 = 'Other Customer Traffic'

            if '%device' in self.statistic:
                data2_stat = percentage_per_timeframe(data2, self.y_axis, id=label2,
                                                      timeframe=self.timeframe)
            else:
                data2_stat = average_per_timeframe(data2, col=self.y_axis, id=label2,
                                                   timeframe=self.timeframe)
                if 'perdevice' in self.statistic:
                    numberdevices = len(data2['deviceid'].unique())
                    data2_stat[self.y_axis] = data2_stat[self.y_axis].apply(lambda x: x / numberdevices)
            data2_stat['color'] = "#033649"
            data2_stat = data2_stat.fillna(0)
            data2_ = data2_stat.reset_index()
            src2 = ColumnDataSource(data=dict(x=data2_[self.x_axis], y=data2_[self.y_axis],
                                              color=data2_['color'], name=data2_['id']))
        else:
            src2 = None
        return src, src2

    def make_plot(self):
        p = figure(plot_height=600, plot_width=600,
                   title=self.title,
                   x_axis_label=self.x_axis_label,
                   y_axis_label=self.y_axis_label,
                   tools=[], toolbar_location=None
                   )

        if self.comparison:
            p.line(source=self.src2, x='x', y='y', color='#033649', line_width=self.width,
                   line_alpha=0.8, legend='name')
            p.circle(source=self.src2, x='x', y='y', color='#033649', fill_color="white", size=5,
                     fill_alpha=0.8, hover_fill_color='#033649')

        p.line(source=self.src, x='x', y='y', color=self.color, line_width=self.width, legend='name')
        p.circle(source=self.src, x='x', y='y', color=self.color, fill_color="white", size=5, fill_alpha=0.8,
                 hover_fill_color=self.color)

        p.y_range.start = 0
        p.legend.label_text_font_size = '8pt'
        p.legend.padding = 5
        p.legend.spacing = 1

        hover = HoverTool(tooltips=[('Traffic', '@name'),
                                    ('Value', '@y')])
        p.add_tools(hover)

        return p

    def dropdown_callback(self, attr, old, new):
        self.y_axis = self.dropdown.value.split(',')[0]
        self.statistic = self.dropdown.value

        if '%device' in self.statistic:
            self.plot.title.text = "Percentage Compare"
        else:
            self.plot.title.text = 'Average Compare'

        new_src1, new_src2 = self.make_dataset()
        self.plot.yaxis.axis_label = self.dropdown.value
        self.src.data.update(new_src1.data)
        if self.comparison:
            self.src2.data.update(new_src2.data)

    def data_dropdown_callback(self, attr, old, new):

        if 'justdevice' in self.statistic:
            self.device = self.device_dropdown.value

        self.id = self.data_dropdown.value
        new_src1, new_src2 = self.make_dataset()
        self.src.data.update(new_src1.data)
        if self.comparison:
            self.src2.data.update(new_src2.data)

    def timeframe_dropdown_callback(self, attr, old, new):
        if self.timeframe_dropdown.value == 'Hour':
            self.timeframe = 'H'
        elif self.timeframe_dropdown.value == 'Half Hour':
            self.timeframe = '30T'
        elif self.timeframe_dropdown.value == 'Day':
            self.timeframe = 'D'
        elif self.timeframe_dropdown.value == 'Week':
            self.timeframe = '7D'
        elif self.timeframe_dropdown.value == 'Month':
            self.timeframe = '30D'
        new_src1, new_src2 = self.make_dataset()
        self.src.data.update(new_src1.data)
        if self.comparison:
            self.src2.data.update(new_src2.data)

    def device_dropdown_callback(self, attr, old, new):
        self.device = self.device_dropdown.value
        new_src1, new_src2 = self.make_dataset()
        self.src.data.update(new_src1.data)
        if self.comparison:
            self.src2.data.update(new_src2.data)

    def get(self):
        return self.plot
