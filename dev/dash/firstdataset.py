from __future__ import division

from histogram import Histogram
from barchart import BarChart, NestedBarChart
from linechart import LineChart
from textbox import TextBox

import numpy as np

from sklearn.neighbors import KernelDensity
from data import Data

from bokeh.io import curdoc
from bokeh.models import WidgetBox
from bokeh.models.widgets import Select
from bokeh.layouts import column, row, layout
from bokeh.plotting import output_file

tools = 'pan'
#output_file('customer_dashboard.html')


def perc_change(new, old):
    """
    Helper for calculating percentage change
    """
    return 100*(new - old)/old


def kde(x, x_grid, bandwidth=0.2, **kwargs):
    """
    Kernel Density Estimation with Scikit-learn
    """
    kde_skl = KernelDensity(bandwidth=bandwidth, **kwargs)
    kde_skl.fit(x[:, np.newaxis])
    # score_samples() returns the log-likelihood of the samples
    log_pdf = kde_skl.score_samples(x_grid[:, np.newaxis])
    return np.exp(log_pdf)


def get_dict_by_id(df, id):
    """
    Split a datatable into a dictionary by a given id field
    """
    ids = df[id].unique()
    d = {}
    for c in ids:
        d[c] = df[df[id] == c]
    return d


data = Data()
initial_y = 'httptrafficamount'
initial_customerid = data.cust_metric_df.iloc[0]['customerid']
customer_list_combo = data.cust_metric_df['customerid'].unique().tolist()\
                      + data.device_metric_df['customerid'].unique().tolist()
customer_list = list(set(customer_list_combo))
customer_dropdown = Select(value=initial_customerid, title='Customer ID', options=customer_list)

df = data.cust_metric_df

menu = [('httptrafficamount', 'Customer Metric: HTTP Traffic Compare'),
        ('httpstrafficamount', 'Customer Metric: HTTPS Traffic Compare'),
        ('caphitcnt', 'Customer Metric: Cap Hit Count Compare'),
        ('datausage', 'Device Metric: Data Usage Compare'),
        ('roamingdataamount', 'Device Metric: Roaming Data Compare'),
        ('capreached', 'Device Metric: Cap Reached Compare'),
        ('policyblockhitcnt', 'Device Metric: Policy Block Hit Compare'),
        ('apps', 'Device Metric: Apps Compare'),
        ('sites', 'Device Metric: Sites Compare'),
        ('datausage,perdevice', 'Device Metric: Per Device Avg Data Usage Compare'),
        ('roamingdataamount,perdevice', 'Device Metric: Per Device Avg Roaming Data Compare'),
        ('capreached,perdevice', 'Device Metric: Per Device Avg Cap Reached Compare'),
        ('policyblockhitcnt,perdevice', 'Device Metric: Per Device Avg Policy Block Hit Compare'),
        ('apps,perdevice', 'Device Metric: Per Device Avg Apps Compare'),
        ('sites,perdevice', 'Device Metric: Per Device Avg Sites Compare'),
        ('capreached,%device', 'Device Metric: Percentage of Devices Cap Reached Compare'),
        ('policyblockhitcnt,%device', 'Device Metric: Percentage of Devices Policy Block Hit Compare')
        ]
dropdown = Select(value=initial_y, title='Statistic', options=menu)

hist = Histogram(data, initial_customerid, statistic=initial_y, y_axis_label=initial_y, bins=10, density=False,
                 dropdown=dropdown, data_dropdown=customer_dropdown)

bc1 = BarChart(data, id=initial_customerid, statistic=initial_y, x_axis='id', title='Average Compare',
               y_axis_label=initial_y,
               dropdown=dropdown, data_dropdown=customer_dropdown)

lc = LineChart(data, id=initial_customerid, statistic=initial_y, x_axis='index', title='Per Hour Average Compare',
               comparison=True,
               y_axis_label=initial_y,
               dropdown=dropdown, data_dropdown=customer_dropdown)

nbc1 = NestedBarChart(data, id=initial_customerid, statistic=initial_y, x_1='id', x_2='index',
                      title='Per Hour Average Compare',
                      y_axis_label=initial_y,
                      dropdown=dropdown, data_dropdown=customer_dropdown)

#text_http = """
#        <center> Customer: <font color="#36A138">{}</font> is on average using <font color="#36A138">{:.2f}MB {}</font> http data
#        and <font color="#36A138">{:.2f}MB {}</font> https data than the average of all other customers. </center>
#        """.format(initial_customerid, abs(http_abs_diff), http_gl, abs(https_abs_diff), https_gl)
#headline = """<center> Customer: <font color="#36A138">{}</font></center>""".format(initial_customerid)

#tb1 = TextBox(headline, font_size_str='250')
#tb2 = TextBox(text_http, font_size_str='200')

dashboard = layout([[WidgetBox(customer_dropdown), WidgetBox(dropdown)],
                    [hist.get()],
                    [bc1.get(), lc.get()],
                    [[nbc1.get()]]
                    ], sizing_mode='stretch_both')

curdoc().add_root(dashboard)
curdoc().title = "dash"
