from __future__ import division
import pandas as pd
import numpy as np
from dash_utilities import Wandera_Green, average_per_timeframe
from bokeh.plotting import figure
from bokeh.models import HoverTool, ColumnDataSource


class Histogram:
    def __init__(self, data, id, statistic,
                 x_axis_label='', y_axis_label='', bins=10, timeframe='H',
                 density=False, color=Wandera_Green, dropdown=None, data_dropdown=None, timeframe_dropdown=None,
                 device_dropdown=None):
        self.data = data
        self.id = id
        self.statistic = statistic
        self.y_axis = statistic.split(',')[0]
        self.bins = bins
        self.density = density
        self.color = color
        self.dropdown = dropdown
        self.data_dropdown = data_dropdown
        self.y_axis_label = y_axis_label
        self.x_axis_label = x_axis_label
        self.timeframe_dropdown = timeframe_dropdown
        self.timeframe = timeframe
        self.device_dropdown = device_dropdown.dropdown
        self.device = self.device_dropdown.value

        self.src = self.make_histogram_dataset()
        self.p = self.make_histogram()

        if self.device_dropdown is not None:
            self.device_dropdown.on_change("value", self.device_dropdown_callback)

        if self.timeframe_dropdown is not None:
            self.timeframe_dropdown.on_change("value", self.timeframe_dropdown_callback)

        if self.dropdown is not None:
            self.dropdown.on_change("value", self.histogram_callback)

        if self.data_dropdown is not None:
            self.data_dropdown.on_change("value", self.histogram_data_callback)

    def make_histogram_dataset(self):
        df = self.data.df_choice(self.statistic)

        if 'justdevice-customer' in self.statistic:
            temp = df[df['customerid'] == self.id]
            df1 = temp[temp['deviceid'] == self.device]
            label = 'Chosen Device'
        elif 'justdevice' in self.statistic:
            df1 = df[df['deviceid'] == self.device]
            label = 'Chosen Device'
        else:
            df1 = df[df['customerid'] == self.id]
            label = 'Customer Traffic'

        data1 = average_per_timeframe(df1, col=self.y_axis, id=label, timeframe=self.timeframe)
        data1['color'] = self.color

        if 'justdevice-customer' in self.statistic:
            temp = df[df['customerid'] == self.id]
            df2 = temp[temp['deviceid'] != self.device]
            label2 = 'Other Devices Average Within Customer'
        elif 'justdevice' in self.statistic:
            df2 = df[df['deviceid'] != self.device]
            label2 = 'Other Devices'
        else:
            df2 = df[df['customerid'] != self.id]
            label2 = 'Other Customer Traffic'

        data2 = average_per_timeframe(df2, col=self.y_axis, id=label2, timeframe=self.timeframe)
        data2['color'] = "#033649"

        percent = 99
        percentile = np.percentile(df[self.y_axis], percent)

        hist_range = (0, percentile)
        #  min(min(data2[self.y_axis]),
        #    min(data1[self.y_axis]))

        hist1, edges1 = np.histogram(data1[self.y_axis], density=self.density, bins=self.bins, range=hist_range)
        hist2, edges2 = np.histogram(data2[self.y_axis], density=self.density, bins=self.bins, range=hist_range)
        bins = min(len(hist1), len(hist2))

        if bins < 5:
            bins = 5

        dataset = pd.DataFrame(columns=['hist', 'left', 'right', 'name', 'color'])
        if '%device' not in self.statistic:
           # for frame in [data2, data1]:
           #     hist, edges = np.histogram(frame[self.y_axis], density=self.density, bins=self.bins, range=hist_range)
           #     arr_df = pd.DataFrame({'hist': hist, 'left': edges[:-1], 'right': edges[1:]})
           #     if 'perdevice' in self.statistic:
           #         numberdevices = len(frame['deviceid'].unique())
           #         arr_df['hist'] = arr_df['hist'].apply(lambda x: x/numberdevices)
           #     arr_df['name'] = frame.iloc[0]['id']
           #     arr_df['color'] = frame.iloc[0]['color']
           #     dataset = dataset.append(arr_df)

            hist1, edges1 = np.histogram(data1[self.y_axis], density=self.density, bins=bins, range=hist_range)
            arr_df1 = pd.DataFrame({'hist': hist1, 'left': edges1[:-1], 'right': edges1[1:]})
            if 'perdevice' in self.statistic:
                numberdevices1 = len(df1['deviceid'].unique())
                arr_df1['hist'] = arr_df1['hist'].apply(lambda x: x / numberdevices1)
            arr_df1['name'] = data1.iloc[0]['id']
            arr_df1['color'] = data1.iloc[0]['color']
            dataset = dataset.append(arr_df1)

            hist2, edges2 = np.histogram(data2[self.y_axis], density=self.density, bins=bins, range=hist_range)
            arr_df2 = pd.DataFrame({'hist': hist2, 'left': edges2[:-1], 'right': edges2[1:]})
            if 'perdevice' in self.statistic:
                numberdevices2 = len(df2['deviceid'].unique())
                arr_df2['hist'] = arr_df2['hist'].apply(lambda x: x / numberdevices2)
            arr_df2['name'] = data2.iloc[0]['id']
            arr_df2['color'] = data2.iloc[0]['color']
            dataset = dataset.append(arr_df2)

        return ColumnDataSource(dataset)

    def make_histogram(self):
        p = figure(x_axis_label=self.x_axis_label,
                   y_axis_label=self.y_axis_label,
                   title="",
                   tools="box_zoom, reset",
                   toolbar_location="above")

        p.quad(source=self.src, bottom=0, top='hist', left='left', right='right',
               color='color', fill_alpha=0.5,
               hover_fill_color='color', hover_fill_alpha=0.9,
               line_color='black',
               legend='name')

        p.y_range.start = 0
        p.x_range.start = 0
        p.xgrid.grid_line_color = None
        p.legend.label_text_font_size = '8pt'
        p.legend.padding = 5
        p.legend.spacing = 1

        hover = HoverTool(tooltips=[('Traffic', '@name'),
                                    ('Value', '@hist')])
        p.add_tools(hover)
        return p

    def histogram_callback(self, attr, old, new):
        self.y_axis = self.dropdown.value.split(',')[0]
        self.statistic = self.dropdown.value

        if '%device' in self.statistic:
            self.p.title.text = "Histogram not suitable for this metric"
        else:
            self.p.title.text = 'Average Compare'

        new_src = self.make_histogram_dataset()
        self.p.xaxis.axis_label = self.dropdown.value
        self.src.data.update(new_src.data)

    def histogram_data_callback(self, attr, old, new):

        if 'justdevice' in self.statistic:
            self.device = self.device_dropdown.value

        self.id = self.data_dropdown.value
        new_src = self.make_histogram_dataset()
        self.src.data.update(new_src.data)

    def timeframe_dropdown_callback(self, attr, old, new):
        print(self.timeframe_dropdown.value)
        if self.timeframe_dropdown.value == 'Hour':
            self.timeframe = 'H'
        elif self.timeframe_dropdown.value == 'Half Hour':
            self.timeframe = '30T'
        elif self.timeframe_dropdown.value == 'Day':
            self.timeframe = 'D'
        elif self.timeframe_dropdown.value == 'Week':
            self.timeframe = '7D'
        elif self.timeframe_dropdown.value == 'Month':
            self.timeframe = '30D'
            print('UPDATING SRC')
        new_src = self.make_histogram_dataset()
        self.src.data.update(new_src.data)

    def device_dropdown_callback(self, attr, old, new):
        self.device = self.device_dropdown.value
        new_src = self.make_histogram_dataset()
        self.src.data.update(new_src.data)

    def get(self):
        return self.p
